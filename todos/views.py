from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from .forms import TodoForm, TodoItemForm

# Create your views here.
def todo_list_list(request):
    todo = TodoList.objects.all()
    context = {
        "todo_object": todo,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": todo,
    }
    return render(request, "todos/detail.html", context)

def new_todo_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm(instance=todo)

    context = {
        "form": form,

    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        todo.delete()
        return redirect('todo_list')
    else:
        context = {'todo': todo}
        return render(request, 'todos/delete.html', context)

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create_todo.html", context)

def todo_item_update(request, id):
    todo = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=todo)

    context = {
        "form": form,

    }
    return render(request, "todos/edit_item.html", context)
